import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

class App extends Component {
	render() {
		return (
			<div className="App">
				<header className="App-header">
					<img src={logo} className="App-logo" alt="logo" />
					<h1 className="App-title">Welcome to React</h1>
				</header>
				<p className="App-intro">
					To get started, edit <code>src/App.js</code> and save to reload.
				</p>
				<Counter />
			</div>
		);
	}
}

export default App;

class Counter extends Component {
	state = {
		angka: 0
	};

	tambah = () => {
		this.setState({
			angka: this.state.angka + 1
		});
	};

	kurang = () => {
		if (this.state.angka <= 0) {
			return alert("Gk boleh 0");
		} else {
			this.setState({
				angka: this.state.angka - 1
			});
		}
	};
	render() {
		return (
			<div>
				<h1>{this.state.angka}</h1>
				<button onClick={this.tambah}>tambah</button>
				<button onClick={this.kurang}>kurang</button>
			</div>
		);
	}
}
